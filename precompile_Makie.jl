using GLMakie

using PackageCompiler

create_sysimage(
    :GLMakie; 
    sysimage_path="MakieSys.so", 
    precompile_execution_file=joinpath("./AbstractPlotting_runtests_stripped.jl")
)